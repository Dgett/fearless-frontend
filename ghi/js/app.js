function createCard(name, description, pictureUrl, startDate, endDate, subtitle) {
  //const column =  document.queryselector('.container' )
  // title = name
  return `
  
  <div class='col'>
    <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
        <p class="card-text">${description}</p>
        
        <footer class="card-footer">${startDate} - ${endDate}</footer>
      </div>
      
    </div>
    
  </div>
  `;
}
function handleAlert(){
return`

      <div class="alert alert-primary" role="alert">
        bad response
      </div>
      `;
}



window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      const alert= document.querySelector('main');
            alert.innerHTML += handleAlert();
      //console.log("response is bad!");
    } else {
      const data = await response.json();

      for (let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok){
          const details = await detailResponse.json();
          const title = details.conference.name;
          const subtitle = details.conference.location.name;
          const description = details.conference.description;
          //console.log(details);
          const startDate = Date(details.conference.starts);
          //console.log(startDate)
          const endDate = Date(details.conference.ends);
          //console.log(endDate)
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl, startDate, endDate, subtitle)
          const column = document.querySelector('.row');
          column.innerHTML += html;
          //console.log(details);

        }
      }

        //const conference = data.conferences[0];
      
        //const nameTag = document.querySelector('.card-title');
        //nameTag.innerHTML = conference.name;
        //const descriptionTag = document.querySelector('.card-text');
        //descriptionTag.innerHTML = Conferencedescription;

        //const picture = details.conference.location.picture_url;
        //const pictureTag = document.querySelector('.card-img-top');
        //pictureTag.src = details.conference.location.picture_url;
        //descriptionTag.innerHTML = picture;

        
    }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

  });
